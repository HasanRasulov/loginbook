<%@page import="java.util.HashSet"%>
<%@page import="java.util.stream.Collectors"%>
<%@page import="java.util.Set"%>
<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@page import="com.fasterxml.jackson.core.type.TypeReference"%>
<%@page import="java.util.List"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.net.URISyntaxException"%>
<%@page import="java.io.File"%>
<%@page import="java.net.URL"%>
<%@page import="loginbook.entity.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script type="text/javascript">

            function sendDeleteToAdmin(id) {

                let adminUrl = "http://localhost:8080/LoginBookJDBC/admin";
                let xmlHttp = new XMLHttpRequest();
                xmlHttp.open("DELETE", adminUrl + "?id=" + id, false);
                xmlHttp.setRequestHeader('Content-type', 'application/json');
                xmlHttp.send(null);

            }

            function sendPutToAdmin(book) {

                $('input[name="title"]').val(book.title);
                $('input[name="genre"]').val(book.genre);
                $('input[name="summary"]').val(book.summary);
                $('input[name="year"]').val(book.year);
                $('input[name="author"]').val(book.author.toString());

                $('<input>').attr({
                    type: 'hidden',
                    name: 'id'
                }).appendTo('form');
                
                $('input[name="id"]').val(book.id);
                $('input[name="addupdate"]').val("Update");

            }

        </script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin page</title>
    </head>
    <body>

        <%

            if (session.getAttribute("name") == null) {
                response.sendRedirect("Login.jsp");
            }
            if ((boolean) session.getAttribute("isAdmin") == false) {
                response.sendRedirect("User.jsp");
            }
        %>


        <ul>
            <c:forEach items="${books}" var="b">

                <li>${b.toString()}                                      
                    <input type="submit" value="Delete" onclick="sendDeleteToAdmin(${b.getId()})"/>
                    <input type="submit" value="Update" onclick="sendPutToAdmin({
                                'id': '${b.getId()}',
                                'title': '${b.getTitle()}',
                                'genre': '${b.getGenre()}',
                                'summary': '${b.getSummary()}',
                                'year': '${b.getYear()}',
                                'author': '${b.getAuthor()}'
                            })"/>
                </li> 

            </c:forEach>

        </ul>


        <form method="post" action="/LoginBookJDBC/admin" id="addUpdateForm">
            <fieldset><legend>Books:</legend>

                <label for="title">Title:
                    <input type="text" name="title" value = ""/>
                </label>
                <label for="genre">Genre:
                    <input type="text" name="genre" value = ""/>
                </label>
                <label for="summary">Summary:
                    <input type="text" name="summary" value = ""/>
                </label>
                <label for="year">Year:
                    <input type="text" name="year" value = ""/>
                </label>
                <label for="author">Author:
                    <input type="text" name="author" value = ""/>
                </label>


            </fieldset>
            <input type="submit" name="addupdate" value="Add"/>
        </form>



    </body>
</html>
