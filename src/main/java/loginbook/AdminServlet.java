package loginbook;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import loginbook.entity.Book;
import loginbook.services.BookDAO;

@WebServlet("/admin")
public class AdminServlet extends HttpServlet {

    private void fetchBooks(HttpServletRequest req, HttpServletResponse resp) {
        BookDAO dao;
        List<Book> books = null;

        try {
            dao = new BookDAO();
            books = dao.getAll();
            dao.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AdminServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        req.setAttribute("books", books);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        fetchBooks(req, resp);
        RequestDispatcher rd = req.getRequestDispatcher("Admin.jsp");
        rd.forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Object id = req.getParameter("id");

        Book book = null;
        BookDAO dao = null;

        if (id == null) {

            book = new Book();

            book.setId(new Random().nextInt(1000));
            book.setTitle((String) req.getParameter("title"));
            book.setGenre((String) req.getParameter("genre"));
            book.setSummary((String) req.getParameter("summary"));
            book.setYear(Integer.parseInt(req.getParameter("year")));
            book.setAuthor(Arrays.asList(req.getParameter("author")));

            try {
                dao = new BookDAO();
                dao.save(book);
                dao.close();
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(AdminServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {

            int book_id = Integer.parseInt((String) id);

            try {
                dao = new BookDAO();
                book = dao.getBookById(book_id);
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(AdminServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            book.setTitle((String) req.getParameter("title"));
            book.setGenre((String) req.getParameter("genre"));
            book.setSummary((String) req.getParameter("summary"));
            book.setYear(Integer.parseInt(req.getParameter("year")));
            book.setAuthor(Arrays.asList(req.getParameter("author")));

            try {
                dao.updateBook(book);
                dao.close();
            } catch (SQLException ex) {
                Logger.getLogger(AdminServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        
        fetchBooks(req, resp);
        RequestDispatcher rd = req.getRequestDispatcher("Admin.jsp");
        rd.forward(req, resp);

    }

    
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int book_id = Integer.parseInt((String) req.getParameter("id"));

        try {
            BookDAO dao = new BookDAO();
            dao.deleteBook(book_id);
            dao.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AdminServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        fetchBooks(req, resp);
        RequestDispatcher rd = req.getRequestDispatcher("Admin.jsp");
        rd.forward(req, resp);

    }

}
