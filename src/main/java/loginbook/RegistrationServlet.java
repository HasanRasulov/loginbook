package loginbook;

import loginbook.entity.*;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import loginbook.services.UserDAO;

@WebServlet("/register")
public class RegistrationServlet extends HttpServlet {
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        resp.setContentType("text/html");
        
        User user = new User();
        user.setId(new Random().nextInt());
        user.setName((String) req.getParameter("name"));
        user.setSurname((String) req.getParameter("sur"));
        user.setPassword((String) req.getParameter("pass"));
        user.setAdress((String) req.getParameter("adr"));
        user.setGender(req.getParameter("gender").equalsIgnoreCase("male") ? Gender.MALE : Gender.FEMALE);
        
        
        UserDAO dao = null;
        
        try {
            dao = new UserDAO();
            dao.save(user);
            dao.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(RegistrationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        resp.sendRedirect("Login.jsp");
        
    }
    

}
