package loginbook.services;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import loginbook.entity.Gender;
import loginbook.entity.Role;
import loginbook.entity.User;

public class UserDAO {

    private PreparedStatement stat;
    private final Connection con;

    public UserDAO() throws ClassNotFoundException, SQLException {

        Class.forName("com.mysql.jdbc.Driver");
        String propFile = "dbproperties.properties";

        String connectionUrl = "";
        Properties prop = new Properties();

        try (InputStream in = UserDAO.class.getClassLoader().getResourceAsStream(propFile)) {

            prop.load(in);

            connectionUrl += "jdbc:mysql://" + prop.getProperty("db.url") + ":" + prop.getProperty("db.port") + "/" + prop.getProperty("db.name") + "?"
                    + "useUnicode=" + prop.getProperty("db.useUnicode") + "&" + "useJDBCCompliantTimezoneShift=" + prop.getProperty("db.useJDBCCompliantTimezoneShift") + "&"
                    + "useLegacyDatetimeCode=" + prop.getProperty("db.useLegacyDatetimeCode") + "&" + "serverTimezone=" + prop.getProperty("db.serverTimezone") + "&" + "useSSL=" + prop.getProperty("db.useSSL");

        } catch (IOException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        con = DriverManager.getConnection(
                connectionUrl, prop.getProperty("db.username"), prop.getProperty("db.password"));

    }

    public void save(User user) throws SQLException {
        stat = con.prepareStatement("insert into users values(?,?,?,?,?,?,?,?)");
        stat.setInt(1, user.getId());
        stat.setString(2, user.getName());
        stat.setString(3, user.getSurname());
        stat.setString(4, user.getAdress());
        stat.setString(5, user.getGenre());
        stat.setString(6, user.getGender().name());
        stat.setString(7, user.getPassword());
        stat.setString(8, user.getRole().name());

        stat.execute();

    }

    public User getUser(String name) throws SQLException, ClassNotFoundException {

        stat = con.prepareStatement("select * from users where name='" + name + "'");

        BookDAO bookDao = new BookDAO();

        User user;
        try (ResultSet rs = stat.executeQuery()) {
            user = new User();
            while (rs.next()) {

                user.setId(rs.getInt("id"));
                user.setName(rs.getString("name"));
                user.setSurname(rs.getString("surname"));
                user.setAdress(rs.getString("adress"));
                user.setGenre(rs.getString("genre"));
                user.setGender(rs.getString("gender").equalsIgnoreCase("MALE") ? Gender.MALE : Gender.FEMALE);
                user.setPassword(rs.getString("password"));
                user.setRole(rs.getString("role").equalsIgnoreCase("User") ? Role.USER : Role.ADMIN);

            }
        }

        return user;
    }

    public void close() throws SQLException {
        stat.close();
        con.close();
    }

}
