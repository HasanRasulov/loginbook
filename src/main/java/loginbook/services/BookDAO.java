package loginbook.services;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import loginbook.entity.Book;

public class BookDAO {

    private PreparedStatement stat;
    private final Connection con;

    public BookDAO() throws ClassNotFoundException, SQLException {

        Class.forName("com.mysql.jdbc.Driver");
        String propFile = "dbproperties.properties";

        String connectionUrl = "";
        Properties prop = new Properties();

        try (InputStream in = UserDAO.class.getClassLoader().getResourceAsStream(propFile)) {

            prop.load(in);

            connectionUrl += "jdbc:mysql://" + prop.getProperty("db.url") + ":" + prop.getProperty("db.port") + "/" + prop.getProperty("db.name") + "?"
                    + "useUnicode=" + prop.getProperty("db.useUnicode") + "&" + "useJDBCCompliantTimezoneShift=" + prop.getProperty("db.useJDBCCompliantTimezoneShift") + "&"
                    + "useLegacyDatetimeCode=" + prop.getProperty("db.useLegacyDatetimeCode") + "&" + "serverTimezone=" + prop.getProperty("db.serverTimezone") + "&" + "useSSL=" + prop.getProperty("db.useSSL");

        } catch (IOException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        con = DriverManager.getConnection(
                connectionUrl, prop.getProperty("db.username"), prop.getProperty("db.password"));

    }

    public void save(Book book) throws SQLException {
        stat = con.prepareStatement("insert into books values(?,?,?,?,?,?)");
        stat.setInt(1, book.getId());
        stat.setString(2, book.getTitle());
        stat.setString(3, book.getGenre());
        stat.setString(4, book.getSummary());
        stat.setInt(5, book.getYear());

        String authors = "";
        authors = book.getAuthor().stream().map((a) -> a + ",").reduce(authors, String::concat);
        stat.setString(6, authors);

        stat.execute();

    }

    public Book getBookById(int id) throws SQLException {
        stat = con.prepareStatement("select * from books where id='" + id + "'");
        Book book = new Book();;
        try (ResultSet rs = stat.executeQuery()) {

            if (rs.next()) {

                book.setId(id);
                book.setTitle(rs.getString("title"));
                book.setGenre(rs.getString("genre"));
                book.setSummary(rs.getString("summary"));
                book.setYear(rs.getInt("year"));
                book.setAuthor(Arrays.asList(rs.getString("authors").split(",")));

            }
        }
        return book;
    }

    public List<Book> getAll() throws SQLException {

        stat = con.prepareStatement("select * from books");
        List<Book> books;
        try (ResultSet rs = stat.executeQuery()) {
            books = new ArrayList<>();
            while (rs.next()) {

                Book book = new Book();

                book.setId(rs.getInt("id"));
                book.setTitle(rs.getString("title"));
                book.setGenre(rs.getString("genre"));
                book.setSummary(rs.getString("summary"));
                book.setYear(rs.getInt("year"));
                book.setAuthor(Arrays.asList(rs.getString("authors").split(",")));

                books.add(book);
            }
        }
        return books;

    }

    public void deleteBook(int id) throws SQLException {

        stat = con.prepareStatement("delete from books where id = '" + id + "'");

        stat.execute();

    }

    public void updateBook(Book book) throws SQLException {

        String authors = "";
        authors = book.getAuthor().stream().map((a) -> a + ",").reduce(authors, String::concat);

        stat = con.prepareStatement("update books set title = " + "'" + book.getTitle() + "'," + "genre=" + "'" + book.getGenre() + "',"
                + "summary=" + "'" + book.getSummary() + "'," + "year=" + "'" + book.getYear() + "',"
                + "authors=" + "'" + authors + "' where id='" + book.getId() + "'");

        stat.executeUpdate();

    }

    public void close() throws SQLException {

        stat.close();
        con.close();

    }

}
