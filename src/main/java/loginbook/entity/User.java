package loginbook.entity;

import java.io.Serializable;
import java.util.List;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class User implements Serializable {

    private int id;
    private String name;
    private String surname;
    private String adress;
    private String genre;
    private Gender gender;
    private String password;
    private List<Book> books;
    private Role role;

}
