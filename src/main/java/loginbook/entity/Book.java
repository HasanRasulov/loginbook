package loginbook.entity;


import java.io.Serializable;
import java.util.List;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Book implements Serializable{

    private int id;
    private String title;
    private String genre;
    private String summary;
    private int year;
    private List<String> author;
     
    
}