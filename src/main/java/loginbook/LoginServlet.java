package loginbook;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import loginbook.entity.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import loginbook.services.BookDAO;
import loginbook.services.UserDAO;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    public static byte tries = 2;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html");

        HttpSession session = req.getSession();

        String name = (String) req.getParameter("name");
        String password = (String) req.getParameter("pass");

        UserDAO dao = null;
        User user = null;
        try {
            dao = new UserDAO();
            user = dao.getUser(name);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }

        session.setAttribute("user", user);

        if (tries == 0) {
            try (PrintWriter out = resp.getWriter()) {
                out.println("You are out of tries");
            }
        } else if (user != null && password.equals(user.getPassword())) {

            session.setAttribute("name", name);

            if (user.getRole() == Role.ADMIN) {

                session.setAttribute("isAdmin", true); 
                resp.sendRedirect("/LoginBookJDBC/admin");

            } else {
                
                session.setAttribute("isAdmin", false);
                resp.sendRedirect("/LoginBookJDBC/user");
            }

        } else {
            --tries;
            resp.sendRedirect("Login.jsp");
        }
    }
    

}
