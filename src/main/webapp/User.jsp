<%@page import="java.util.HashSet"%>
<%@page import="java.util.stream.Collectors"%>
<%@page import="java.util.Set"%>
<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@page import="com.fasterxml.jackson.core.type.TypeReference"%>
<%@page import="java.util.List"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.net.URISyntaxException"%>
<%@page import="java.io.File"%>
<%@page import="java.net.URL"%>
<%@page import="loginbook.entity.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script type="text/javascript">

            function addToCard(book) {
                let d = new Date();
                d.setTime(d.getTime() + (1000 * 24 * 60 * 60 * 1000));
                let expires = "expires=" + d.toGMTString();
                let cookie = book.id + "=" + book.title + " " + book.genre + " " + book.summary + " "
                        + book.year + " " + book.author.toString() + ";"
                        + expires + ";path=/LoginBookJDBC/user";
                document.cookie = cookie;
                $('input[id="add_' + book.id + '"]').hide();


            }

            function deleteFromCard(bookId) {

                document.cookie = bookId + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/LoginBookJDBC/user";
                $('input[id="add_' + bookId + '"]').show();

            }


            function showList() {

                $('ul[id="list"]').show();
                let str = document.cookie.toString();
                $('ul[id="list"]').empty();

                const reg = /[0-9]=/;

                let cookies = str.split(';');

                let i = 0;
                while (cookies.length > i) {
                    let match = reg.exec(cookies[i]);
                    if (!match)
                        break;
                    let node = cookies[i].substring(match.index + 2);
                    //if() check if node is still present in add_xxx ul
                    //then append
                    $('ul[id="list"]').append("<li>" + node + "</li>");
                    i++;

                }


            }

        </script>   
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Page</title>
    </head>
    <body>
        <%

            if (session.getAttribute("name") == null) {
                response.sendRedirect("Login.jsp");
            }
        %>

        <ul>
            <c:forEach items="${books}" var="b">

                <li>${b.toString()}                                      
                    <input type="submit" value="V" id="add_${b.getId()}" onclick="addToCard({
                                'id': '${b.getId()}',
                                'title': '${b.getTitle()}',
                                'genre': '${b.getGenre()}',
                                'summary': '${b.getSummary()}',
                                'year': '${b.getYear()}',
                                'author': '${b.getAuthor()}'
                            })"/>
                    <input type="submit" value="X" name="delete" onclick="deleteFromCard(${b.getId()})"/>
                </li> 

            </c:forEach>

        </ul>

        <input type="submit" value="ShowList" id="showlist"  onclick="showList()" /> 

        <ul id="list" hidden>

        </ul>

    </body>
</html>
